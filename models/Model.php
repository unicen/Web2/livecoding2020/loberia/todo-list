<?php

class Model {

    private $db;

    public function __construct() {
        $this->db = $this->create_connection();
    }

    public function getDb() {
        return $this->db;
    }

    public function create_connection() {
        $db = new PDO('mysql:host=localhost;dbname=db_todo_list;charset=utf8', 'root', '');
        $host = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'db_todo_list';

        try {
            $db = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $userName, $password);
            
            // Solo en modo desarrollo
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $db;        
    }
}
