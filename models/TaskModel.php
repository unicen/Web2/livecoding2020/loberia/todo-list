<?php

require_once('Model.php');

class TaskModel extends Model {

    /**
     * @return array
     * Retorna todas las tareas guardadas en la tabla task
    **/
    public function getAll() {
        $query = $this->getDb()->prepare('SELECT * FROM task ORDER BY completed, priority ASC');
        $query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function getTasksByPriority($priority) {
        $query = $this->getDb()->prepare('SELECT * FROM task WHERE priority = ?');
        $query->execute([$priority]);
        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function save($title, $priority, $description, $image = null) {
        $pathImg = null;
        if ($image)
            $pathImg = $this->uploadImage($image);
        
        $query = $this->getDb()->prepare('INSERT INTO task (title, priority, description, image) VALUES (?, ?, ?, ?)');
        return $query->execute([$title, $priority, $description, $pathImg]);
    }

    private function uploadImage($image) {
        $target = 'uploads/tasks/' . uniqid() . '.jpg';
        move_uploaded_file($image, $target);
        return $target;
    }

    /**
     * @param $id
     * @return mixed
     * Retorna una tupla de la tabla task a partir de un id pasado x parametro
     */
    function getTask($id){
        $query = $this->getDb()->prepare('SELECT * FROM task WHERE id = ?');
        $query->execute(array(($id)));
        return $query->fetch(PDO::FETCH_OBJ);
    }

    /**
     * @param $id
     * Finaliza una task a partir de un id pasado x parametro
     */
    function endTaskDB($id) {
        $query = $this->getDb()->prepare('UPDATE task SET completed = 1 WHERE id = ?');
        $query->execute([$id]);
    }

    /**
     * @param $id
     * Elimina una tupla de la tabla task a partir de un id pasado x parametro
     */
    function deleteTask($id) {
        $query = $this->getDb()->prepare('DELETE FROM task WHERE id = ?');
        $query->execute([$id]);
    }

    function updateTask($taskId, $title, $priority, $description) {
        $query = $this->getDb()->prepare(
            'UPDATE task SET title = ?, description = ?, priority = ? WHERE id = ?');
        $result = $query->execute([$title, $description, $priority, $taskId]);
        return $result;
    }

}