<?php

require_once('Model.php');

class UserModel extends Model {

    public function getUserByUsername($username){
        $query = $this->getDb()->prepare('SELECT * FROM `user` WHERE username = ?');
        $query->execute(array(($username)));
        return $query->fetch(PDO::FETCH_OBJ);
    }
}