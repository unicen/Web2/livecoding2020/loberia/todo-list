{include 'templates/header.tpl'}
<div class="container task-detail">
    <div class="row">
        <div class="col-md-9">

            <h1> {$task->title} </h1>
            <p> {$task->description} </p>
            <p><b>Priority: </b> {$task->priority} </p>
            {if $task->completed eq true}
                <p> Finished work </p>
            {else}
                <p> Task in progress </p>
            {/if}
            {if isset($task->image)}
                <img src="{$task->image}"/>
            {/if}
            <p><a href="{$home}">Back</a></p>
        </div>
        <div class="col-md-3">
            {include 'vue/asideTaskList.vue'}
        </div>
    </div>
</div>
    <script src="js/main.js"></script>
    {include 'templates/footer.tpl'}
