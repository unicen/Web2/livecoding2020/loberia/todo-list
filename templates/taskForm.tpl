<form action="new" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label>Title</label>
        <input name="title" type="text" class="form-control">
    </div>

    <div class="form-group">
        <label>Priority</label>
        <select name="priority" class="form-control">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
    </div>

    <div class="form-group">
        <label>Description</label>
        <textarea name="description" type="text" class="form-control" rows="3"></textarea>
    </div>

    <div class="form-group">
        <input type="file" name="input_name" id="imageToUpload">
    </div>

    <button type="submit" class="btn btn-primary">Save</button>
</form>