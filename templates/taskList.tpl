{include 'templates/header.tpl'}

<div class="container">
    <h1>Task List</h1>
    {include 'templates/taskForm.tpl'}
    <ul class="list-group list-padd">
        {foreach from=$tasks item=task}
            <li class="list-group-item {if $task->completed eq true}completed{/if}">
                <b>{$task->title}</b> - {$task->description}
                <a class="btn btn-info" href="task/{$task->id}">Ver</a>
                <a class="btn btn-danger" href="delete/{$task->id}"> Delete </a>
                {if $task->completed eq false}
                    <a class="btn btn-warning" href="completed/{$task->id}"> Completed </a>
                {/if}
            </li>
        {/foreach}
    </ul>
</div>

{include 'templates/footer.tpl'}
