<?php

class AuthHelper {

    public function __construct() {
    }

    /**
     * Inicia la session solo si no está iniciada.
     * Corrige el problema de "session ya iniciada".
     */
    static private function start() {
        if (session_status() != PHP_SESSION_ACTIVE)
            session_start();
    }


    public static function getLoggedUserName() {
        self::start();
        if (isset($_SESSION['USERNAME'])){
            return $_SESSION['USERNAME'];
        }
        else {
            return false;
        }
    }

    public static function checkLoggedIn(){
        self::start();
        if(!isset($_SESSION['ID_USER'])){
            header('Location: ' . BASE_URL . "login");
            die;
        }
    }

    /**
     * Inicia la session y guarda los datos dal usuario
     */
    static public function login($user) {
        self::start();
        $_SESSION['IS_LOGGED'] = true;
        $_SESSION['ID_USER'] = $user->id_usuario;
        $_SESSION['USERNAME'] = $user->username;
    }

    /**
     * Destruye la session
     */
    public static function logout() {
        self::start();
        session_destroy();
    }

}