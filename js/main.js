// definimos la app Vue
let taskListComponent = new Vue({
    el: "#app-tasks",
    data: {
        loading: true,
        footer: "Este panel se renderiza con CSR ;)",
        counter: 0,
        tasks: []
    },
    methods: {
        openPopup: function(id) {
            alert("hola: " + id);
        }
    }
});

// asigno event listener al boton de refresh
document.querySelector('#btn-refresh').addEventListener('click', load);

// carga inicial de las tareas
load();

function load() {
    taskListComponent.loading = true;
    fetch('api/task')
        .then(response => response.json())
        .then(tareas => {
            // asigno las tareas que me devuelve la API
            taskListComponent.tasks = tareas; // es como el $this->smarty->assign("tasks", tareas);
            taskListComponent.loading = false;
        });
}
