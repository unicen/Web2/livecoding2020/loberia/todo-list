<?php

include_once('views/UserView.php');
include_once('models/UserModel.php');

class UserController {

    private $model;

    private $view;

    public function __construct() {
        $this->view = new UserView();
        $this->model = new UserModel();
    }

    public function showLogin(){
        $this->view->showLogin();
    }

    public function verify() {
        if(!empty($_POST['username']) && !empty($_POST['password'])){
            $user = $_POST['username'];
            $pass = $_POST['password'];
            $userDb = $this->model->getUserByUsername($user);

            if(!empty($userDb) && password_verify($pass, $userDb->password)){
                AuthHelper::login($userDb);
                header('Location: ' . BASE_URL . "tasks");
            }
            else {
                $this->view->showLogin("Login incorrecto");
            }
        }
    }

    public function logout() {
        AuthHelper::logout();
        header("Location: " . BASE_URL . 'login');
    }

}