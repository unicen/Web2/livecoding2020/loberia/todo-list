<?php

include_once('models/TaskModel.php');
include_once('views/TaskView.php');

class TaskController {

    private $model;
    private $view;

    public function __construct() {
        $this->model = new TaskModel();
        $this->view = new TaskView();
    }

    /**
     * Muestra la lista de tareas.
    **/
    public function showTasks() {
        $tasks = $this->model->getAll();
        $this->view->showTasks($tasks);
    }

    /**
     * Agrega una nueva tarea a la lista.
     */
    public function addTask() {
        $title = $_POST['title'];
        $priority = $_POST['priority'];
        $description = $_POST['description'];

        if (empty($title) || empty($priority)) {
            $this->view->showError("Faltan datos obligatorios");
            die();
        }
        if($priority == 5) {
            $priority5 = $this->model->getTasksByPriority($priority);
            if(!empty($priority5)) {
                $this->view->showError("No puede ingresar mas tareas con prioridad 5");
                die();
            }
        }

        if($_FILES['input_name']['type'] == "image/jpg" || $_FILES['input_name']['type'] == "image/jpeg" 
            || $_FILES['input_name']['type'] == "image/png") {
            $success = $this->model->save($title, $priority, $description, $_FILES['input_name']['tmp_name']);
        } else {
            $success = $this->model->save($title, $priority, $description);
        }        

        if($success)
            header('Location: ' . BASE_URL . "tasks");
        else
            $this->view->showError("Faltan datos obligatorios");
    }

    /**
     * @param $id
     * @return string
     * Contruye el html para visualizar una tarea
    **/
    public function showTask($id) {
        $task = $this->model->getTask($id);
        $this->view->showTask($task);
    }

    /**
     * @param $id
     * Finaliza una tarea
    **/
    public function endTask($id) {
        AuthHelper::checkLoggedIn();
        $this->model->endTaskDB($id);
        header('Location: ' . BASE_URL . "tasks"); 
    }

    /**
     * @param $id
     * Elimina una tarea
     */
    function deleteTask($id) {
        AuthHelper::checkLoggedIn();
        $this->model->deleteTask($id);
        header("Location: ../tasks");
    }

}