# todo-list

App para lista de tareas. Web 2 - 2020 Loberia

## Base de datos

Dump guardado en la carpeta '/database'. 

#### Importar dump
1. Crear base de datos desde phpmyadmin.
2. Importar archivo desde la opcion 'Importar' seleccionando nuestro archivo 'db_todo_list.sql'


## Guardado de imagenes

Para el guardado de imagenes crear la carpeta upload/tasks en la raiz del proyecto
  