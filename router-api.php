<?php
require_once 'libs/router/Router.php';
require_once 'api/TaskApiController.php';

// creo el ruteador usando la libreria externa
$router = new Router();

// creo la tabla de ruteo
$router->addRoute('task', 'GET', 'TaskApiController', 'getTask');
$router->addRoute('task/:ID', 'GET', 'TaskApiController', 'getTask');
$router->addRoute('task/:ID', 'DELETE', 'TaskApiController', 'deleteTask');
$router->addRoute('task', 'POST', 'TaskApiController', 'addTask');
$router->addRoute('task/:ID', 'PUT', 'TaskApiController', 'updateTask');



// rutea
$router->route($_REQUEST['resource'], $_SERVER['REQUEST_METHOD']);
