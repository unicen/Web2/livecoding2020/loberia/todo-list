<?php

require_once('View.php');
include_once('helpers/auth.helper.php');

class TaskView extends View {

    private $smarty;

    public function __construct() {
        parent::__construct();
        $authHelper = new AuthHelper();
        $username = $authHelper->getLoggedUserName();
        $this->getSmarty()->assign('username', $username);
    }

    /**
     * @return string
     * Contruye el html para visualizar las tareas guardadas en la bd
     */
    function showTasks($tasks) {
        $this->getSmarty()->assign('title', 'Home');
        $this->getSmarty()->assign('tasks', $tasks);
        $this->getSmarty()->assign('home', BASE_URL.'tasks');

        $this->getSmarty()->display('templates/taskList.tpl');
    }

    /**
     * @param $task 
     * Muestra una tarea pasada por parámetro
    **/
    function showTask($task) {
        $this->getSmarty()->assign('title', 'Task Detail');
        $this->getSmarty()->assign('task', $task);
        $this->getSmarty()->assign('home', BASE_URL.'tasks');

        $this->getSmarty()->display('templates/taskDetail.tpl');
    }

    /**
    * Muestra errores por pantalla
    */
    public function showError($msg) {
        $this->getSmarty()->assign('message', $msg);
        $this->getSmarty()->assign('title', 'Error');
        $this->getSmarty()->assign('home', BASE_URL.'tasks');

        $this->getSmarty()->display('templates/error.tpl');
    }

}