<?php

require_once ('controllers/TaskController.php');
require_once ('controllers/UserController.php');

define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

if ($_GET['action'] == '')
    $_GET['action'] = 'tasks';


//$_GET['action'] = 'parametro1/parametro2'
$urlParts = explode('/', $_GET['action']);
//$urlParts = ['parametro1', 'parametro2']

switch ($urlParts[0]) {
    case 'login':
        $controller = new UserController();
        $controller->showLogin();
        break;
    case 'verify':
        $controller = new UserController();
        $controller->verify();
        break;
    case "logout":
        $controller = new UserController();
        $controller->logout();
    case 'tasks':
        $controller = new TaskController();
        $controller->showTasks();
        break;
    case 'task':
        $controller = new TaskController();
        $controller->showTask($urlParts[1]);
        break;
    case 'new':
        $controller = new TaskController();
        $controller->addTask();
        break;
    case 'delete':
        $controller = new TaskController();
        $controller->deleteTask($urlParts[1]);
        break;
    case 'completed':
        $controller = new TaskController();
        $controller->endTask($urlParts[1]);
        break;
    default:
        echo "<h1>Error 404 - Page not found </h1>";
        break;
}